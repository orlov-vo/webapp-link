<?php
defined('LINK_MAX_LENGTH') or define('LINK_MAX_LENGTH', 6);

class LinkForm extends CFormModel
{
	public $link;

	public function rules()
	{
		return array(
			array('link', 'required'),
			array('link', 'url'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'link' => Yii::t('Link', 'Link'),
		);
	}
} 
