<?php

class Link extends CActiveRecord
{
	public $hash;
	public $link;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'tbl_links';
	}

	public function rules()
	{
		return array(
			array('hash', 'unique'),
			array('link', 'required'),
			array('link', 'url'),
		);
	}

	public function generateHash()
	{
		$hash = "";
		$valid_chars =
			"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		$num_valid_chars = strlen($valid_chars);

		for ($i = 0; $i < LINK_MAX_LENGTH; $i++) {
			$random_pick = mt_rand(1, $num_valid_chars);
			$random_char = $valid_chars[$random_pick - 1];
			$hash .= $random_char;
		}

		$this->hash = $hash;
		return $this->hash;
	}
} 
