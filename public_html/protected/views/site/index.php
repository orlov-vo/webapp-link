<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle = Yii::app()->name;
?>

<?php if (Yii::app()->user->hasFlash('link')): ?>

	<div class="flash-success">
		<?php echo Yii::app()->user->getFlash('link'); ?>
	</div>

<?php else: ?>

	<p><?php echo Yii::t('Link', 'On this page you can make of a long and complex links simple. Such links are easier to use in your records and reports.'); ?></p>

	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'contact-form',
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
		),
	)); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'link'); ?>
		<?php echo $form->textField($model, 'link', array(
			'class' => 'field',
		)); ?>
		<?php echo $form->error($model, 'link'); ?>
	</div>

	<div class="btn-group">
		<?php echo CHtml::ajaxSubmitButton(Yii::t('Link', 'Submit'),
			CHtml::normalizeUrl(array('site/ajaxLink')), array(
				'error' => "js:function() {
					console.error('Error');
				}",
				'success' => "js:function(data) {
					var obj = $.parseJSON(data);
					if (obj.error === 0) {
						$('#short_url').val(window.location.protocol + '//' +
						window.location.host + '/' + obj.hash)
						.removeClass('hidden');
					} else {
						console.error('Error #' + obj.error);
						$('#short_url').addClass('hidden');
					}
				}",
			), array(
				'class' => 'btn',
			)); ?>
	</div>

	<div class="row">
		<?php echo CHtml::textField('short_url', '', array(
			'id' => 'short_url',
			'class' => 'field hidden',
			'readonly' => 'true',
		)); ?>
	</div>

	<?php $this->endWidget(); ?>

<?php endif; ?>
