<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::app()->language; ?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="<?php echo Yii::app()->language; ?>" />

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div id="page">

	<header>
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</header>

	<?php echo $content; ?>

	<footer>
		Copyright &copy; <?php echo date('Y'); ?> by
		<a href="mailto:orlov@tesjin.ru">Vladislav Orlov</a>.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</footer>

</div>

</body>
</html>
