<?php
return array(
	'Link' => 'Ссылка',
	'On this page you can make of a long and complex links simple. Such links are easier to use in your records and reports.' => 'На этой странице Вы можете сделать из длинной и сложной ссылки простую. Такие ссылки удобнее использовать в Ваших записях и сообщениях.',
	'Submit' => 'Готово',
);
