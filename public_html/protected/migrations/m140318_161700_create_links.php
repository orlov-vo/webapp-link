<?php

class m140318_161700_create_links extends CDbMigration
{
	public function up()
	{
		$this->createTable('tbl_links', array(
            'id' => 'pk',
            'hash' => 'string NOT NULL',
            'link' => 'string NOT NULL',
        ));
	}

	public function down()
	{
		$this->dropTable('tbl_links');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}