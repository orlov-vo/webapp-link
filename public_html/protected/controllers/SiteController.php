<?php

class SiteController extends Controller
{
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$model = new LinkForm;
		$this->render('index', array('model' => $model));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionAjaxLink()
	{
		$form = new LinkForm;
		if (isset($_POST['LinkForm'])) {
			$form->attributes = $_POST['LinkForm'];
			if ($form->validate()) {
				$model = new Link;
				$model->link = $form->link;

				$criteria = new CDbCriteria;
				$criteria->condition = 'link = :link';
				$criteria->params = array(':link' => $model->link);
				$obj = Link::model()->find($criteria);
				if ($obj != NULL) {
					echo CJSON::encode(array(
						'error' => 0,
						'hash' => $obj->hash,
						'original' => $form->link,
					));
				} else {

					do {
						$model->generateHash();
						$criteria = new CDbCriteria;
						$criteria->condition = 'hash = :hash';
						$criteria->params = array(':hash' => $model->hash);
					} while (Link::model()->exists($criteria));


					if ($model->save()) {
						echo CJSON::encode(array(
							'error' => 0,
							'hash' => $model->hash,
							'original' => $form->link,
						));
					} else {
						echo CJSON::encode(array(
							'error' => 3,
						));
					}
				}
			} else {
				echo CJSON::encode(array(
					'error' => 2,
				));
			}
		} else {
			echo CJSON::encode(array(
				'error' => 1,
			));
		}
		Yii::app()->end();
		return true;
	}

	public function actionUrl($hash)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'hash = :hash';
		$criteria->params = array(':hash' => $hash);
		$obj = Link::model()->find($criteria);
		if ($obj != NULL) {
			$this->redirect($obj->link);
		} else {
			throw new CHttpException(404, 'Указанная ссылка не найдена');
		}
	}
}
